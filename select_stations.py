import sys
import os
import numpy as num
from pyrocko import util, model, weeding, cake
from pyrocko.plot import mpl_init, mpl_color
from pyrocko.fdsn import station as fs

from matplotlib import pyplot as plt, cm, colors, gridspec
from mpl_toolkits.basemap import Basemap

util.setup_logging('select_stations.py', 'warning')


nwanted = int(sys.argv[1])
neighborhood = int(sys.argv[2])

dn_meta = 'meta'
fnout_stations = os.path.join(
        dn_meta,
        'stations-selected-%dnb%d.txt' % (nwanted, neighborhood))

fnout_badnesses = os.path.join(
        dn_meta,
        'stations-selected-badnesses-%dnb%d.txt' % (nwanted, neighborhood))

lat_min = -22.
lat_max = -12.
lon_min = -177.
lon_max = -169.


lat_center = (lat_max + lat_min) / 2.
lon_center = (lon_max + lon_min) / 2.

ev = model.Event(lat=lat_center, lon=lon_center)

def read_badnesses(fn):
    nsl_to_badness = {}
    with open(fn, 'r') as f:
        for line in f:
            nsl, v = line.split()
            nsl = tuple(nsl.split('.'))
            v = float(v)
            nsl_to_badness[nsl] = v

    return nsl_to_badness


sites = ('geofon', 'iris')

fn_station_candidates = 'station_candidates.txt'

nsl_to_badness = read_badnesses('station_badnesses.txt')
nsls = sorted(nsl_to_badness.keys())

if not os.path.exists(fn_station_candidates):
    sxs = {}
    for site in sites:
        fn = 'meta/stations-complete-%s.xml' % site
        sxs[site] = fs.load_xml(filename=fn)

    stations = {}
    for site in sites:
        stations_site = sxs[site].get_pyrocko_stations(nsls=nsls)
        for station in stations_site:
            nsl = station.nsl()
            if nsl not in stations:
                stations[nsl] = station

    stations = stations.values()
    stations.sort(key=lambda station: station.nsl())

    model.dump_stations(stations, fn_station_candidates)

else:
    stations = model.load_stations(fn_station_candidates)


for station in stations:
    station.set_event_relative_data(ev)

stations_choice = weeding.weed_stations(
    stations,
    nwanted,
    badnesses_nsl=nsl_to_badness,
    neighborhood=neighborhood)[0]

# #############################################################################
# dump the badnesses of the selected stations to a file

tlist = []
for station in stations_choice:
    nsl = station.nsl()
    sta = '.'.join(nsl[:2])
    val = nsl_to_badness[nsl]
    dist = station.dist_deg
    tlist.append((sta, val, dist))

tlist.sort(key=lambda t: t[1], reverse=True)

with open(fnout_badnesses, 'w') as f:
    for sta, val, dist in tlist:
        f.write(str("%s    %8.4f    %8.4f\n" % (sta.ljust(8), val, dist)))


# #############################################################################
# write (pyrocko) stations file

stations_choice.sort(key=lambda station: station.nsl())

model.dump_stations(stations_choice, fnout_stations)

# #############################################################################
# plot stations map

fontsize = 9.
mpl_init(fontsize=fontsize)

mappable = cm.ScalarMappable(
    norm=colors.Normalize(0., 7.),
    cmap=plt.get_cmap('rainbow'))

mappable.set_array(num.zeros(0))

m = Basemap(
        width=200.*cake.d2m,
        height=200.*cake.d2m,
        lat_0=lat_center,
        lon_0=lon_center,
        projection='aeqd')

m.drawmapboundary(fill_color=mpl_color('white'))
m.drawcoastlines(linewidth=0.5)
m.fillcontinents(color='0.8')
m.drawparallels(num.arange(-90,91,10))
m.drawmeridians(num.arange(-180,181,10))

nsl_choice = set(station.nsl() for station in stations_choice)


for station in stations:
    badness = nsl_to_badness[station.nsl()]
    color = mappable.to_rgba(badness)
    xpt, ypt = m(station.lon, station.lat)
    if station.nsl() in nsl_choice:
        m.plot(xpt, ypt, '^', color=color, mew=2.5, markersize=12)

    else:
        m.plot(xpt, ypt, '^', color=color, mew=0.1, markersize=12)


total_badness = 0.
for station in stations_choice:
    badness = nsl_to_badness[station.nsl()]
    total_badness += badness

print "Total Badness: ", total_badness


plt.show()
