#!/bin/bash


from=$1
to=$2
npar=$3


echo Started at: $(date)
echo .......................................


eventnames=$(awk -v a=$from -v b=$to 'NR>=a && NR<=b {print $1}' event-names.txt)

grond go bodywave.gronf $eventnames --status=quiet --parallel=$npar


echo .......................................
echo Stopped at: $(date)
