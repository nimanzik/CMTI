import math
import sys
import os.path as op
import numpy as num
from pyrocko import crust2x2, cake, util, weeding, model, gf
from pyrocko.fdsn import ws
from pyrocko.fomosto import qseis as qseis_mod

km = 1000.


def init_store(store_id, distance_min, distance_max, distance_delta, s_model, r_model):

    store_dir = op.join('gf_stores', store_id)

    qseis = qseis_mod.QSeisConfig()

    qseis.time_region = (
            gf.meta.Timing('first(P|depth_p)-100'),
            gf.meta.Timing('last(S|depth_s|sS)+200'))

    qseis.cut = (
            gf.meta.Timing('first(P|depth_p)-100'),
            gf.meta.Timing('last(S|depth_s|sS)+200'))

    qseis.fade = (
            gf.meta.Timing('first(P|depth_p)-100'),
            gf.meta.Timing('first(P|depth_p)-30'),
            gf.meta.Timing('last(S|depth_s|sS)+130'),
            gf.meta.Timing('last(S|depth_s|sS)+200'))

    qseis.qseis_version = '2006a'

    qseis.relevel_with_fade_in = True

    qseis.sw_algorithm = 1
    qseis.slowness_window = (0., 0.03, 0.15, 0.20)

    qseis.wavelet_duration_samples = 0.001
    qseis.sw_flat_earth_transform = 1

    config = gf.meta.ConfigTypeA(
            id = store_id,
            ncomponents = 10,
            sample_rate = 0.5,
            receiver_depth = 0.*km,
            source_depth_min = 8.0*km,
            source_depth_max = 700.0*km,
            source_depth_delta = 4*km,
            distance_min = distance_min,
            distance_max = distance_max,
            distance_delta = distance_delta,
            earthmodel_1d = s_model,
            earthmodel_receiver_1d = r_model,
            modelling_code_id = 'qseis.2006a',
            tabulated_phases = [
                    gf.meta.TPDef(
                        id = 'P',
                        definition = 'P'),
                    gf.meta.TPDef(
                        id = 'depth_p',
                        definition = 'p'),
                    gf.meta.TPDef(
                        id = 'S',
                        definition = 'S'),
                    gf.meta.TPDef(
                        id = 'depth_s',
                        definition = 's'),
                    gf.meta.TPDef(
                        id = 'pP',
                        definition = 'pP'),
                    gf.meta.TPDef(
                        id = 'sS',
                        definition = 'sS'),
            ])

    config.validate()
    return gf.store.Store.create_editables(store_dir, config=config, extra={'qseis': qseis})

lat_min = -22.
lat_max = -12.
lon_min = -177.
lon_max = -169.

nstations_wanted = 10

lat_center = (lat_max + lat_min) / 2.
lon_center = (lon_max + lon_min) / 2.

lats = num.linspace(lat_min, lat_max, 6)
lons = num.linspace(lon_min, lon_max, 5)

needed = set()
for lat in lats:
    for lon in lons:
        p = crust2x2.get_profile(lat, lon)
        waterdepth = - p.elevation()
        waterdepth_approx = round(waterdepth / 2000.) * 2000.
        needed.add((p._ident, waterdepth_approx))

source_models = []
for ident, waterdepth in sorted(needed):
    p = crust2x2.get_profile(ident)
    p.set_layer_thickness(crust2x2.LWATER, waterdepth)
    mod = cake.load_model(crust2_profile=p).extract(depth_max='cmb')
    cake.write_nd_model(mod, 'model_%s_%.0f.nd' % (ident, waterdepth))

    source_models.append((ident, waterdepth, mod))


minradius_deg = 30.
maxradius_deg = 90.
distance_delta = 4.*km

stations_fn = 'stations.txt'

if op.exists(stations_fn):
    stations = model.load_stations(stations_fn)

else:
    tmin = util.str_to_time('2016-01-01 00:00:00')
    tmax = util.str_to_time('2016-02-01 00:00:00')
    site = 'iris'

    sx = ws.station(
        site=site,
        startbefore=tmax,
        endafter=tmin,
        format='text',
        level='channel',
        includerestricted=False,
        latitude=lat_center,
        longitude=lon_center,
        minradius=minradius_deg,
        maxradius=maxradius_deg,
        channel='BH?')

    stations_all = sx.get_pyrocko_stations()

    event = model.Event(lat=lat_center, lon=lon_center)

    for station in stations_all:
        station.set_event_relative_data(event)

    stations = weeding.weed_stations(stations_all, nstations_wanted)[0]

    model.dump_stations(stations, 'stations.txt')

needed = set()
for station in stations:
    p = crust2x2.get_profile(station.lat, station.lon)
    print p._ident
    needed.add(p._ident)

sys.exit()

receiver_models = []
for ident in sorted(needed):
    p = crust2x2.get_profile(ident)
    p.set_layer_thickness(crust2x2.LWATER, 0.0)
    mod = cake.load_model(crust2_profile=p).extract(depth_max=120000.)
    receiver_models.append((ident, mod))

for s_ident, s_waterdepth, s_model in source_models:
    for r_ident, r_model in receiver_models:

        distance_min = math.floor(minradius_deg * cake.d2m / distance_delta) * distance_delta
        distance_max = math.ceil(maxradius_deg * cake.d2m / distance_delta) * distance_delta

        store_id = 'tonga_%s_%.0f_%s' % (s_ident, s_waterdepth, r_ident)
        init_store(
            store_id=store_id,
            distance_min=distance_min,
            distance_max=distance_max,
            distance_delta=distance_delta,
            s_model=s_model,
            r_model=r_model)

global_model = cake.load_model().extract(depth_max='cmb')

init_store(
    store_id='tonga_ak135',
    distance_min=distance_min,
    distance_max=distance_max,
    distance_delta=distance_delta,
    s_model=global_model,
    r_model=None)

