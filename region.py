import numpy as np

from pyrocko import cake, crust2x2, model, util, weeding
from pyrocko.fdsn import ws


# #############################################################################
# predefined input arguments
lat_min = -22.
lat_max = -12.
lon_min = -177.
lon_max = -169.

nlats = 6
nlons = 5

nstations_wanted = 10

site = 'iris'
tmin = '2016-01-01 00:00:00'
tmax = '2016-02-01 00:00:00'
radius_min = 30.
radius_max = 90.
channel = 'BH?'

# #############################################################################

# constants
km = 1000.


# source side models
lats = np.linspace(lat_min, lat_max, nlats)
lons = np.linspace(lon_min, lon_max, nlons)

needed = set()
for lat in lats:
    for lon in lons:
        p = crust2x2.get_profile(lat, lon)
        waterdepth = -1. * p.elevation()
        waterdepth_approx = round(waterdepth / 2000.) * 2000.
        needed.add((p._ident, waterdepth_approx))

source_models = []
for ident, waterdepth in sorted(needed):
    p = crust2x2.get_profile(ident)
    p.set_layer_thickness(crust2x2.LWATER, waterdepth)
    mod = cake.load_model(crust2_profile=p)
    mod = mod.extract(depth_max='cmb')
    cake.write_nd_model(mod, 'model_%s_%.0f.nd' % (ident, waterdepth))

    source_models.append((ident, waterdepth, mod))

# get stations
tmin = util.str_to_time(tmin)
tmax = util.str_to_time(tmax)

lat_center = (lat_min + lat_max) / 2.
lon_center = (lon_min + lon_max) / 2.

sx = ws.station(
        site=site,
        startbefore=tmin,
        endafter=tmax,
        format='text',
        level='channel',
        includerestricted=False,
        latitude=lat_center,
        longitude=lon_center,
        minradius=radius_min,
        maxradius=radius_max,
        channel=channel)

stations_all = sx.get_pyrocko_stations()

event = model.Event(lat=lat_center, lon=lon_center)

for station in stations_all:
    station.set_event_relative_data(event)

stations = weeding.weed_stations(stations_all, nstations_wanted)[0]

# receiver side models
needed = set()
for station in stations:
    p = crust2x2.get_profile(station.lat, station.lon)
    needed.add(p._ident)

receiver_models = []
for ident in sorted(needed):
    p = crust2x2.get_profile(ident)
    p.set_layer_thickness(crust2x2.LWATER, 0.0)
    mod = cake.load_model(crust2_profile=p)
    mod = mod.extract(depth_max=120.*km)
    receiver_models.append((ident, mod))


# print source-receiver models
models = set()
for s_ident, s_waterdepth, _ in source_models:
    for r_ident, _ in receiver_models:
        models.add((s_ident, s_waterdepth, r_ident))
        print "%s_%.0f_%s" % (s_ident, s_waterdepth, r_ident)

print "Number of unique models: ", len(models)
