#!/bin/bash


dn_data=data
temp=temp-bl-$(date '+%d-%H%M')

host=$1


mkdir ${temp}


for d in `ls ${dn_data}`;do
    mkdir ${temp}/$d
    cp ${dn_data}/$d/stations_blacklist.txt ${temp}/$d/
done


scp -r ${temp} $host:/home/nooshiri/grond_trial/
