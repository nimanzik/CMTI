from collections import defaultdict
from itertools import repeat
import os.path as op
import sys

import numpy as np
from scipy import spatial

import grond
from grond import StationCorrection
from pyrocko import gf, guts, trace
from pyrocko.guts import Float, List, Object, String, Tuple
from pyrocko.parimap import parimap
from pyrocko.util import ensuredir



# Constants for reference ellipsoids used for datum transformation
# (defined by the World Geodetic System 1984 (WGS84))
a = 6378137.0            # semi-major axis [m]
b = 6356752.3142         # semi-minor axis [m]
f = 1./298.257223563     # earth flattening ((a-b)/a))
esq = 6.69437999014e-3   # eccentricity squared of ellipsoid (f(2-f))

km = 1000.

SeismosizerError = gf.SeismosizerError

floatpoint_err_setting = np.seterr(invalid='raise')


class CentroidBest(Object):
    lat = Float.T()
    lon = Float.T()
    depth = Float.T()


class EventStationCorrections(Object):
    event_name = String.T()
    centroid_best = CentroidBest.T()
    station_corrections = List.T(StationCorrection.T())


def correlation_lag(tr_syn, tr_obs):
    c = trace.correlate(
        tr_syn,
        tr_obs,
        mode='same',
        normalization='normal')

    t, _ = c.max()
    return t


def get_station_corrections(rundir, tdelay_max=30.0, use_measure='mean'):

    config = guts.load(filename=op.join(rundir, 'config.yaml'))

    config.set_basepath(rundir)
    problem, xs, misfits = grond.load_problem_info_and_data(rundir, subset='harvest')

    # get dataset
    event_name = problem.base_source.name
    ds = config.get_dataset(event_name)

    # set engine
    problem.set_engine(config.engine_config.get_engine())

    # get best source location
    best_source = grond.core.get_best_source(problem, xs, misfits)
    centroid_best = CentroidBest(
        lat=best_source.lat,
        lon=best_source.lon,
        depth=best_source.depth)

    # get the index of an ensemble of potential sources
    gms = problem.global_misfits(misfits)
    isort = np.argsort(gms)

    for target in problem.targets:
        target.set_dataset(ds)

    payload = []
    for i in isort:
        payload.append((problem, xs[i]))


    nslc_to_tdelay_data = defaultdict(list)
    nslc_to_afactor_data = defaultdict(list)
    for (problem, x) in payload:
        #ds.empty_cache()
        ms, ns, results = problem.evaluate(x, result_mode='full')

        for result in results:
            if not isinstance(result, SeismosizerError):
                obs = result.processed_obs.copy()
                syn = result.processed_syn

                t = correlation_lag(syn, obs)
                if abs(t) <= tdelay_max:
                    nslc_to_tdelay_data[obs.nslc_id].append(t)
                    obs.shift(t)

                alpha = np.sum(syn.ydata*obs.ydata)/np.sum(syn.ydata**2)
                nslc_to_afactor_data[obs.nslc_id].append(alpha)


    nslc_ids = set(nslc_to_tdelay_data.keys())
    nslc_ids.update(nslc_to_afactor_data.keys())

    scs = []
    for nslc_id in nslc_ids:
        try:
            tdelays = np.asarray(nslc_to_tdelay_data[nslc_id], dtype=float)
            tdelays = tdelays[~np.isnan(tdelays)]

            delay = getattr(np, use_measure)(tdelays)
        except (KeyError, FloatingPointError):
            delay = 0.0

        try:
            afactors = np.asarray(nslc_to_afactor_data[nslc_id], dtype=float)
            afactors = afactors[~np.isnan(afactors)]
            factor = getattr(np, use_measure)(afactors)
        except (KeyError, FloatingPointError):
            factor = 1.0

        sc = StationCorrection(
            codes=nslc_id,
            delay=delay,
            factor=factor)
        sc.regularize()
        scs.append(sc)

    ev_sc = EventStationCorrections(
            event_name=event_name,
            centroid_best=centroid_best,
            station_corrections=scs)

    return ev_sc


def dump_station_corrections(station_corrections, filename):
    return guts.dump_all(station_corrections, filename=filename)


def get_dist_weight(d, cutoff, a=3, b=3):
    """
    Calculating distance weight using a biweight function.

    :param d: distance between two points.
    :param cutoff: cutoff distance (maximum seperation).
    :param a, b: exponents defining the shape of the weighting curve (default:
        bicubic function (a = b = 3)).
    :returns w: distance weight value.
    """
    if d > cutoff:
        return 0.0
    return (1 - (d/cutoff)**a)**b


def get_nearest_neighbors(center, data_locs, kmin, kmax, rmax):
    """
    Finding the nearby sources that are located within a sphere of radius
    `rmax` around the `center`.
    Suppose *K* is the total number of events within radius `rmax` around
    `center`, then:
    if K >= kmax, it returns kmax nearest neighbors.
    if kmin <= K < kmax, it returns all K neighbors.
    if K < kmin, it returns None.
    """
    tree = spatial.cKDTree(data_locs)

    # to include sources located exactly at distance rmax
    rmax += 1e-6

    distances, indexes = tree.query(
        center,
        k=kmax,
        eps=0,
        p=2,
        distance_upper_bound=rmax)

    # if found less than kmax neighbors
    if np.any(np.isinf(distances)):
        i_ok = ~np.isinf(distances)
        n_ok = np.sum(i_ok)

        if n_ok < kmin:
            return (None, None)
        else:
            distances = distances[i_ok]
            indexes = indexes[i_ok]


    weights = np.apply_along_axis(
        np.vectorize(get_dist_weight),
        0,
        distances,
        rmax)

    return indexes, weights


def geodetic_to_ecef(lat, lon, alt):
    """
    Converting geodetic coordinates to ECEF (Earth-Centered, Earth-Fixed).
    :param lat: ellipsoidal latitude in *degrees*.
    :param lon: ellipsoidal longitude in *degrees*.
    :param alt: ellipsoidal altitude (height) in *METERS*.
    :returns: coordinates in ECEF (Cartesian) system in *METERS*.
    :rtype: numpy.ndarray
    .. seealso::
        https://en.wikipedia.org/wiki/Geographic_coordinate_conversion#From_geodetic_to_ECEF_coordinates
    """
    lat, lon = np.radians(lat), np.radians(lon)
    # Normal (plumb line)
    N = a / np.sqrt(1 - (esq*np.sin(lat)*np.sin(lat)))

    X = (N+alt) * np.cos(lat) * np.cos(lon)
    Y = (N+alt) * np.cos(lat) * np.sin(lon)
    Z = ((N * (1-esq)) + alt) * np.sin(lat)

    return np.asarray([X, Y, Z])


def get_station_corrections_parimap(
        rundirs,
        tdelay_max=30.0,
        use_measure='mean',
        nprocs=None):

    nr = len(rundirs)
    t_for_parproc = repeat(tdelay_max, nr)
    m_for_parproc = repeat(use_measure, nr)

    processes = parimap(
        get_station_corrections,
        rundirs,
        t_for_parproc,
        m_for_parproc,
        nprocs=nprocs)

    ev_sc_all = list(processes)
    return ev_sc_all


def get_relative_station_corrections(
        ev_sc_all,
        kmin=5,
        kmax=1e3,
        rmax=200*km,
        apply_dist_weight=False):

    source_loc_all = np.asarray([geodetic_to_ecef(
        x.centroid_best.lat,
        x.centroid_best.lon,
        x.centroid_best.depth*-1.0) for x in ev_sc_all])

    # loop over events
    for itar, tar in enumerate(ev_sc_all):
        source_loc_t = source_loc_all[itar]
        indexes_n, weights_n = get_nearest_neighbors(
            source_loc_t,
            source_loc_all,
            kmin+1,
            kmax+1,
            rmax)

        if indexes_n is None:
            for sc in tar.station_corrections:
                sc.delay = 0.0
                sc.factor = 1.0

            continue

        # loop over recording nslcs for the given event
        # sc_t and sc_n are target and neighbor events station corrections, respectively
        for sc_t in tar.station_corrections:
            delays = [sc_t.delay]
            factors = [sc_t.factor]
            weights = [1.0]

            # loop over neighboring events
            for i in xrange(len(indexes_n)):
                index_n = indexes_n[i]
                neighbor = ev_sc_all[index_n]

                try:
                    sc_n = [x for x in neighbor.station_corrections if x.codes==sc_t.codes][0]
                    delays.append(sc_n.delay)
                    factors.append(sc_n.factor)

                    if apply_dist_weight:
                        weights.append(weights_n[i])
                    else:
                        weights.append(1.0)

                except IndexError:
                    continue

            if len(delays) < kmin:
                delay_new = 0.0
                factor_new = 1.0
            else:
                delay_new = float(np.average(delays, weights=weights))
                factor_new = float(np.average(factors, weights=weights))

            # update the station corrections of the target event
            sc_t.delay = delay_new
            sc_t.factor = factor_new
            sc_t.regularize()

    # returns the updated objects
    return ev_sc_all

