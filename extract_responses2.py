import time
import os.path as op
from pyrocko import model, pile, util
from pyrocko.fdsn import station as fs

util.setup_logging('extract_responses2.py', 'warning')

sites = ['geofon', 'iris']
dn_data = 'data'
dn_event_template = op.join(dn_data, '%(event_name)s')
fn_events_selected = 'catalogs/events-selected-gcmt.txt'
dn_raw_template = op.join(dn_event_template, 'observed', '%(site)s')
fn_response_template = op.join(
    'meta', 'responses', '%(site)s',
    'response-%(network)s.%(station)s.%(location)s.%(channel)s.xml')

fn_response_out_template = op.join(
    dn_event_template, 'responses.xml')


events_selection = model.load_events(fn_events_selected)

for ev in events_selection:
    print ev.name

    networks = {}
    stations = {}
    channels = {}
    for site in sites:
        dn_raw = dn_raw_template % dict(
            event_name=ev.name,
            site=site)

        pile_raw = pile.make_pile(dn_raw, fileformat='detect')

        nslcs = pile_raw.nslc_ids.keys()

        for (net, sta, loc, cha) in nslcs:
            fn_resp = fn_response_template % dict(
                site=site,
                network=net,
                station=sta,
                location=loc,
                channel=cha)

            sx = fs.load_xml(filename=fn_resp)

            for (network, station, channel) in \
                    sx.iter_network_station_channels(time=ev.time):

                if net not in networks:
                    networks[net] = network

                if (net, sta) not in stations:
                    stations[net, sta] = station

                channels[net, sta, loc, cha] = channel

    for network in networks.values():
        network.station_list = []

    for (net, sta), station in stations.iteritems():
        station.channel_list = []
        networks[net].station_list.append(station)

    for (net, sta, loc, cha), channel in channels.iteritems():
        stations[net, sta].channel_list.append(channel)

    sx_out = fs.FDSNStationXML(
        source='Merged from IRIS and GEOFON',
        created=time.time(),
        network_list=list(networks.values()))

    fn_resp_out = fn_response_out_template % dict(
        event_name=ev.name)

    sx_out.dump_xml(filename=fn_resp_out)
