import glob
import os.path as op

from pyrocko import guts, model
from station_corrections import *

km = 1000.


kmin = 0
kmax = 100000
rmax = 200000*km


dn_run = 'gruns.run1'
fn_rundir_template = op.join(dn_run, 'bodywave_%(event_name)s.run')


dn_data = 'data'
fn_stacorr_template = op.join(dn_data, '%(event_name)s', 'station_corrections.txt')

fn_events_selected = 'catalogs/events-selected-gcmt.txt'
fn_events_notused = 'catalogs/events-notused.txt'

events_selection = model.load_events(fn_events_selected)

events_notused = open(fn_events_notused, 'r').read().splitlines()

rundirs = []
for event in events_selection:

    if event.name in events_notused:
        continue

    rundir = fn_rundir_template % dict(event_name=event.name)
    rundirs.append(rundir)

rundirs = rundirs[:8]
print rundirs
ev_sc_all = get_station_corrections_parimap(
    rundirs,
    tdelay_max=30.0,
    use_measure='mean')


#ev_sc_all_updated = get_relative_station_corrections(
#    ev_sc_all,
#    kmin=kmin,
#    kmax=kmax,
#    rmax=rmax,
#    apply_dist_weight=False)
#
#
#for ev_sc in ev_sc_all_updated:
#
#    filename = fn_stacorr_template % dict(event_name=ev_sc.event_name)
#    guts.dump_all(ev_sc.station_corrections, filename=filename)
