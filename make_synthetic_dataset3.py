from collections import Counter, defaultdict
import itertools
import os.path as op
import tempfile

import numpy as np

from pyrocko.fdsn import ws
from pyrocko import util, catalog
from pyrocko import model, io, trace, gf, crust2x2, pile, gui_util
from pyrocko.fdsn import station as fs

ws.g_timeout = 120.

crust2_profile_keys = sorted(crust2x2.get_profile_keys())


def get_store_id(slat, slon, rlat, rlon):
    p = crust2x2.get_profile(slat, slon)
    sid = p._ident
    waterdepth = - p.elevation()
    waterdepth_approx = round(waterdepth / 2000.) * 2000.

    p = crust2x2.get_profile(rlat, rlon)
    rid = p._ident

    have = 'A0 B0 B6 G2 H5 P6 R2 T6 U9'.split()

    if rid not in have:
        rid = have[crust2_profile_keys.index(rid) % len(have)]

    return 'tonga_%s_%i_%s' % (sid, waterdepth_approx, rid)


def extend_noise_window(tr):

    a, b = tr.copy(), tr.copy()
    deltat = tr.deltat
    t_shift = (tr.tmax - tr.tmin) * 0.2

    for r in range(10):
        b.shift(t_shift)

        dist = (b.tmin - (a.tmin + (a.data_len() - 1)*deltat)) / deltat
        idist = int(round(dist))

        na = a.ydata.size
        n = -idist + 1

        a.ydata = np.concatenate((a.ydata, b.ydata[n:]))

        taper = 0.5 - 0.5*np.cos(
            (1.+np.arange(n)) / (1.+n)*np.pi)

        a.ydata[na-n:na] = a.ydata[na-n:na] * (1.-taper)
        a.ydata[na-n:na] = a.ydata[na-n:na] + (b.ydata[:n] * taper)

        a.tmax = b.tmax

        if a.mtime and b.mtime:
            a.mtime = max(a.mtime, b.mtime)

    return a


util.setup_logging()

km = 1000.

fmin = 0.01

global_store_id = 'tonga_ak135'


distance_margin = 200 * km
minradius_deg = 2.5
maxradius_deg = 90.

lat_min = -22.
lat_max = -12.
lon_min = -177.
lon_max = -169.


lat_center = (lat_max + lat_min) / 2.
lon_center = (lon_max + lon_min) / 2.


tmin = util.str_to_time('2010-01-01 00:00:00')
tmax = util.str_to_time('2016-10-01 00:00:00')
sites = ('geofon', 'iris')
tstep = 90*24*60*60
nevents = 115

dn_data = 'data'

fn_events = 'catalogs/events-complete-gcmt.txt'
fn_events_selected = 'catalogs/events-selected-gcmt.txt'
fn_response_template = op.join(
    'meta', 'responses', '%(site)s',
    'response-%(network)s.%(station)s.%(location)s.%(channel)s.xml')

dn_event_template = op.join(dn_data, '%(event_name)s')

dn_raw_template = op.join(dn_event_template, 'observed', '%(site)s')
fn_raw_template = op.join(
    dn_raw_template,
    '%(network)s.%(station)s.%(location)s.%(channel)s.%(tmin)s.mseed')

fn_syn_template = op.join(
    dn_event_template, 'synthetic_%(setup)s',
    '%(network)s.%(station)s.%(location)s.%(channel)s.%(tmin)s.mseed')

setups = list(itertools.product(['e1', 'e2'], ['n1', 'n2', 'n3', 'n4']))

noise_factor = {
    'n1': 0.1,
    'n2': 0.5,
    'n3': 1.0,
    'n4': 0.02}

engine = gf.LocalEngine(
    store_superdirs=['/data/masterdes3/heimann/gf_stores_tonga'],
    use_config=True)

global_store = engine.get_store(global_store_id)
target_sample_rate = global_store.config.sample_rate

# see SEED manual v2.4, Appendix A : Channel Naming
# Band Codes:
#   V: very long period (fs = ~0.1 Hz), L: long period (fs = ~1 Hz,)
#   M: mid period (fs = 1-10 Hz), B: broad-band (fs = 10-80 Hz),
#   H: high broad-band (fs = 80-250 Hz)
#
# Instrument Codes:
#   H: high gain seismometer, L: low gain seismometer
#
priority_band_code = ['V', 'L', 'M', 'B', 'H']  # , 'S', 'E']
priority_units = ['M/S', 'M', 'M/S**2']
priority_instrument_code = ['H', 'L']


# for observed seismograms: 'S-P+t_pad' sec before P and 't_pad' sec after S
# for synthetic seismograms: 't_pad/2' sec before P and after S
t_pad = 16*60.   # (in seconds)


# #############################################################################

# downloading the list of events

if not op.exists(fn_events):
    gcmt = catalog.GlobalCMT()
    events = gcmt.get_events(
        time_range=(tmin, tmax),
        latmin=lat_min,
        latmax=lat_max,
        lonmin=lon_min,
        lonmax=lon_max)

    model.dump_events(events, fn_events)
else:
    print 'loading cached events...'
    events = model.load_events(fn_events)
    print 'done'

# #############################################################################

# selecting larger events (N = nevents)

if not op.exists(fn_events_selected):
    events = sorted(events, key=lambda ev: -ev.magnitude)
    events_selection = events[:nevents]
    model.dump_events(events_selection, fn_events_selected)
else:
    print 'loading cached selected events...'
    events_selection = model.load_events(fn_events_selected)
    print 'done'

# #############################################################################

# downloading the list of stations

sxs = {}

for site in sites:
    fn_stations = 'meta/stations-complete-%s.xml' % site

    if not op.exists(fn_stations):
        sxs[site] = ws.station(
            site=site,
            startbefore=tmax,
            endafter=tmin,
            format='text',
            level='channel',
            includerestricted=False,
            latitude=lat_center,
            longitude=lon_center,
            minradius=minradius_deg,
            maxradius=maxradius_deg,
            channel='?H?')

        sxs[site].dump_xml(filename=fn_stations)

    else:
        print 'loading cached fdsn response for %s...' % site.upper()
        sxs[site] = fs.load_xml(filename=fn_stations)
        print 'done'

# #############################################################################

# extracting permanent stations

t = tmin

nsl_count = Counter()

while t < tmax:
    stations_all = {}

    for site in sites:
        sx = sxs[site]
        stations_site = sx.get_pyrocko_stations(timespan=(t, t+tstep))

        for s in stations_site:
            nsl = s.nsl()
            if nsl not in stations_all:
                stations_all[nsl] = s

    for nsl in stations_all:
        nsl_count[nsl] += 1

    t += tstep


nsl_permanent = set()
for nsl in sorted(nsl_count.keys()):
    if nsl_count[nsl] > 14:
        nsl_permanent.add(nsl)


# #############################################################################

sxs_resp = {}

for ievent, event in list(enumerate(events_selection)):

    print '--'*20
    print ievent, event.name

    if False:
        have_data_complete = set()

        for site in sites:

            # #####################################################################

            # selecting channels

            channels = sxs[site].choose_channels(
                target_sample_rate=target_sample_rate,
                priority_band_code=priority_band_code,
                priority_units=priority_units,
                priority_instrument_code=priority_instrument_code,
                time=event.time)

            channels_selected = {}
            for nslc, channel in channels.iteritems():
                if (nslc[:3] in nsl_permanent and
                        nslc[:3] not in have_data_complete):
                    channels_selected[nslc] = channel

            # #####################################################################

            # selecting time range for each channel

            selection = []
            for net, sta, loc, cha in channels_selected.keys():
                channel = channels[net, sta, loc, cha]
                deltat = 1.0 / channel.sample_rate.value

                # source
                source = gf.MTSource.from_pyrocko_event(event)

                # receiver
                rec = gf.Location(
                    lat=channel.latitude.value,
                    lon=channel.longitude.value)

                try:
                    t_p = event.time + global_store.t('{stored:P}', source, rec)
                    t_s = event.time + global_store.t('{stored:S}', source, rec)

                    if t_p and t_s:
                        t_len = t_s - t_p + t_pad
                        tmin_selection = t_p - t_len
                        tmax_selection = t_p + t_len

                        selection.append((
                            net, sta, loc, cha,
                            tmin_selection-10*deltat,
                            tmax_selection+10*deltat))

                except gf.OutOfBounds:
                    pass

            selection.sort()

            # #####################################################################

            # downloading and saving observed data

            ipos = 0
            neach = 50
            have_data = set()

            while ipos < len(selection):

                sub_selection = selection[ipos:ipos+neach]

                for ss in sub_selection:
                    print site, ss

                try:
                    data = ws.dataselect(site=site, selection=sub_selection)

                    f = tempfile.NamedTemporaryFile()

                    while True:
                        buf = data.read(1024)
                        if not buf:
                            break
                        f.write(buf)

                    f.flush()

                    trs = io.load(f.name)
                    for tr in trs:
                        try:
                            have_data.add(tr.nslc_id)

                        except trace.NoData:
                            pass

                    fns2 = io.save(
                        trs,
                        fn_raw_template,
                        additional=dict(event_name=event.name, site=site))

                    f.close()

                except ws.EmptyResult:
                    pass

                ipos += neach


            for nslc in have_data:
                have_data_complete.add(nslc[:3])


            selection_resp = []
            for nslc in sorted(have_data):
                selection_resp.append(nslc + (tmin, tmax))


            for (net, sta, loc, cha, tmin, tmax) in selection_resp:
                try:
                    fn = fn_response_template % dict(
                        site=site,
                        network=net,
                        station=sta,
                        location=loc,
                        channel=cha)

                    if not op.exists(fn):
                        util.ensuredirs(fn)
                        sx = ws.station(
                            site=site,
                            level='response',
                            selection=[(net, sta, loc, cha, tmin, tmax)])

                        sx.dump_xml(filename=fn)

                except ws.EmptyResult:
                    pass


    # #########################################################################

    # calculating synthetic seismograms

    dn_event = dn_event_template % dict(event_name=event.name)

    if op.exists(dn_event):
        for setup in setups:

            nsl_have = set()
            for site in sites:
                print ''.join(setup), site

                # loading and caching observed data (downloaded data)
                dn_raw = dn_raw_template % dict(event_name=event.name, site=site)
                pile_raw = pile.make_pile(dn_raw, fileformat='detect')

                nslcs = pile_raw.nslc_ids.keys()
                nsl_to_c_data = defaultdict(list)

                for nslc in nslcs:
                    nsl_to_c_data[nslc[:3]].append(nslc[3])

                source = gf.MTSource.from_pyrocko_event(event)

                # channels of a computation request (source-station synthetics)
                # including post-processing parameters
                targets = []
                for nsl in sorted(nsl_to_c_data.keys()):
                    nslcs_station = [nsl + (c,) for c in nsl_to_c_data[nsl]]

                    stations = sxs[site].get_pyrocko_stations(
                        nslcs=nslcs_station, time=event.time)

                    # check there is only one station
                    if len(stations) != 1:
                        continue

                    station = stations[0]

                    if setup[0] == 'e2':
                        store_id = get_store_id(
                            source.lat,
                            source.lon,
                            station.lat,
                            station.lon)
                    elif setup[0] == 'e1':
                        store_id = global_store_id


                    store = engine.get_store(store_id)
                    distance = source.distance_to(station)

                    if distance > (store.config.distance_max - distance_margin):
                        continue

                    if distance < (store.config.distance_min + distance_margin):
                        continue

                    net, sta, loc = station.nsl()
                    for channel in station.get_channels():
                        cha = channel.name
                        targets.append(gf.Target(
                            codes=(net, sta, loc, cha),
                            quantity='displacement',
                            lat=station.lat,
                            lon=station.lon,
                            azimuth=channel.azimuth,
                            dip=channel.dip,
                            interpolation='nearest_neighbor',
                            store_id=store_id))

                # Response objects; processing the request of the synthetic
                # seismogram calculations
                response = engine.process(source, targets)

                # iterating over results of request.
                # synthetic traces are named tr.
                markers = []
                for source, target, tr in response.iter_results():
                    print tr.nslc_id,

                    store = engine.get_store(target.store_id)
                    t_p = source.time + store.t('{stored:P}', source, target)
                    t_s = source.time + store.t('{stored:S}', source, target)
                    t_len = t_s - t_p + t_pad/2.
                    t_noise_offset = t_len

                    # cosine taper
                    tap = trace.CosTaper(tr.tmin, t_p, tr.tmax+1., tr.tmax+1.)

                    # observed traces
                    trs_raw = pile_raw.all(
                        trace_selector=lambda tr_raw: tr_raw.nslc_id==tr.nslc_id)


                    if (len(trs_raw) == 1 and
                            trs_raw[0].tmax - trs_raw[0].tmin > t_len):

                        tr_raw = trs_raw[0]

                        try:
                            # loading instrument response files
                            if (site, tr.nslc_id) not in sxs_resp:
                                net, sta, loc, cha = tr.nslc_id
                                fn_resp = fn_response_template % dict(
                                    site=site,
                                    network=net,
                                    station=sta,
                                    location=loc,
                                    channel=cha)

                                sxs_resp[site, tr.nslc_id] = fs.load_xml(
                                    filename=fn_resp)

                            sx_resp = sxs_resp[site, tr.nslc_id]

                            # getting response function
                            resp = sx_resp.get_pyrocko_response(
                                tr.nslc_id,
                                timespan=(tr.tmin, tr.tmax),
                                fake_input_units='M')

                            tr = tr.transfer(transfer_function=resp)
                            tr.taper(tap)

                            tr_raw = tr_raw.copy()
                            pad_raw = 10. * tr_raw.deltat
                            thalf_raw = (tr_raw.tmin + tr_raw.tmax) / 2.
                            noise_win = tr_raw.chop(
                                tr_raw.tmin+pad_raw,
                                thalf_raw-pad_raw,
                                inplace=False)
                            out_tr = extend_noise_window(noise_win)
                            out_tr.ydata[:] = out_tr.ydata[:] * noise_factor[setup[1]]
                            out_tr.downsample_to(tr.deltat)
                            out_tr.add(tr)
                            #tr_raw.ydata[:] = tr_raw.ydata[:] * noise_factor[setup[1]]
                            #tr_raw.shift(t_noise_offset)
                            #tr_raw.downsample_to(tr.deltat)
                            #tr_raw.add(tr)
                            #tr_raw.chop(tr_raw.tmin, t_s+t_pad/2.)

                            io.save(
                                [out_tr],
                                fn_syn_template,
                                additional=dict(
                                    event_name=event.name,
                                    setup=''.join(setup)))


                            markers.append(
                                gui_util.Marker([tr.nslc_id], tr.tmin, t_p))

                            nsl_have.add(tr.nslc_id[:3])

                            print '++ OK ++'

                        except (fs.NoResponseInformation,
                                util.UnavailableDecimation,
                                trace.NoData):
                            print '-- NoK --'
                            pass

                    else:
                        print '-- NoK --'
