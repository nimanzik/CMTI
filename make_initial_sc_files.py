import glob
import os.path as op

from grond import StationCorrection
from pyrocko import guts, model


dn_data = 'data'
fn_stations_template = op.join(dn_data, '%(event_name)s', 'stations.txt')
fn_sc_template = op.join(dn_data, '%(event_name)s', 'station_corrections.txt')

fn_events = 'catalogs/events-selected-gcmt.txt'


events = model.load_events(fn_events)

channels = ['Z', 'T']
for event in events:

    print event.name

    fn_stations = fn_stations_template % dict(event_name=event.name)
    stations = model.load_stations(fn_stations)

    scs = []
    for station in stations:
        nsl = station.nsl()
        for ch in channels:
            nslc = nsl + (ch,)
            scs.append(StationCorrection(codes=nslc, delay=0.0, factor=1.0))

    filename = fn_sc_template % dict(event_name=event.name)
    guts.dump_all(scs, filename=filename)
