
filename = 'catalogs/events-notused.txt'

events_NotUsed_list = [
    '201004042028A', '201101240102A', '201101270059A', '201102032025A',
    '201109222307A', '201205052023A', '201209250306A', '201302070719A',
    '201302100937A', '201312090003A', '201406291824A', '201410280315A',
    '201507061224A', '201507262242A', '201601130555A', '201607232009A']

with open(filename, 'w') as f:
    for event in events_NotUsed_list:
        f.write("%s\n" % event)
