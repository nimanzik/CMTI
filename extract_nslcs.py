import os.path as op

from pyrocko.fdsn import station as fs
from pyrocko import model


dn_data = 'data'
fn_response_template = op.join(dn_data, '%(event_name)s', 'responses.xml')
fn_station_template = op.join(dn_data, '%(event_name)s', 'stations.txt')

fn_events_selected = 'catalogs/events-selected-gcmt.txt'
fn_stations_selected = 'meta/stations-selected-100nb1.txt'


events_selection = model.load_events(fn_events_selected)
stations_selection = model.load_stations(fn_stations_selected)

sxs = [(x.network, x.station) for x in stations_selection]


for ievent, event in list(enumerate(events_selection))[:]:

    print '--'*20
    print ievent, event.name

    respfile = fn_response_template % dict(event_name=event.name)
    loader = fs.load_xml(filename=respfile)
    stations = loader.get_pyrocko_stations()

    have_data = []
    ns_to_c_data = {}
    for station in stations:

        print station.nsl_string()

        net, sta = station.network, station.station
        if (net, sta) in sxs:
            if (net, sta) not in ns_to_c_data.keys():
                ns_to_c_data[net, sta] = set()

            channels = station.get_channel_names()
            if any([ch not in ns_to_c_data[net, sta] for ch in channels]):
                have_data.append(station)
                ns_to_c_data[net, sta].update(channels)

    have_data.sort(key=lambda sta: sta.nsl())

    fn_station = fn_station_template % dict(event_name=event.name)
    model.dump_stations(have_data, fn_station)
