#!/bin/bash


dn_data=data
temp=temp-sc-$(date '+%d-%H%M')

host=$1


mkdir ${temp}


for d in `ls ${dn_data}`;do
    mkdir ${temp}/$d
    cp ${dn_data}/$d/station_corrections.txt ${temp}/$d/
done


scp -r ${temp} $host:/home/nooshiri/grond_trial/
